<?php

declare(strict_types=1);

namespace App\Helper;

class StringGenerator
{
    public function generateString(int $length): string
    {
        $result = '';

        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';

        for ($i = 0; $i < $length; ++$i) {
            $result = $result . $chars[rand(0, (strlen($chars) - 1))];
        }

        return $result;
    }

    public function generateNumbers(int $length): string
    {
        $result = '';

        $chars = '1234567890';

        for ($i = 0; $i < $length; ++$i) {
            $result = $result . $chars[rand(0, (strlen($chars) - 1))];
        }

        return $result;
    }
}
