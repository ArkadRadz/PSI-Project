<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('delivery_street')
            ->add('delivery_postcode')
            ->add('delivery_country', CountryType::class)
            ->add('delivery_city')
            ->add('submit', SubmitType::class, [
                'label' => 'Checkout',
            ])
        ;
    }
}
