<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Item;
use App\Entity\ItemCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemCategoryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Category')
            ->add('Item_ID', EntityType::class, [
                'class' => Item::class,
                'label' => 'Items in category',
                'multiple' => 'true',
                'expanded' => 'true',
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ItemCategory::class,
        ]);
    }
}
