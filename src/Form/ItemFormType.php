<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Wood;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Positive;

class ItemFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('images', CollectionType::class, [
                'entry_type' => FileType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false,
                'delete_empty' => true,
                'required' => false,
            ])
            ->add('description')
            ->add('width', IntegerType::class, [
                'constraints' => [new Positive()],
            ])
            ->add('height', IntegerType::class, [
                'constraints' => [new Positive()],
            ])
            ->add('length', IntegerType::class, [
                'constraints' => [new Positive()],
            ])
            ->add('item_amount', IntegerType::class)
            ->add('item_price', MoneyType::class, [
                'constraints' => [new Positive()],
                'currency' => false,
            ])
            ->add('wood', EntityType::class, [
                'class' => Wood::class,
                'multiple' => 'true',
                'expanded' => 'true',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Add',
            ])
        ;
    }
}
