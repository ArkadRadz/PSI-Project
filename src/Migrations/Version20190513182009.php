<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190513182009 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('sqlite' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE "order" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id_id INTEGER NOT NULL, sum_price NUMERIC(10, 0) NOT NULL, order_date VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_F52993989D86650F ON "order" (user_id_id)');
        $this->addSql('CREATE TABLE item_category (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE item_category_item (item_category_id INTEGER NOT NULL, item_id INTEGER NOT NULL, PRIMARY KEY(item_category_id, item_id))');
        $this->addSql('CREATE INDEX IDX_782CF782F22EC5D4 ON item_category_item (item_category_id)');
        $this->addSql('CREATE INDEX IDX_782CF782126F525E ON item_category_item (item_id)');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, uuid VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, login VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, nip VARCHAR(255) NOT NULL, company_account BOOLEAN NOT NULL, company_name VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649D17F50A6 ON user (uuid)');
        $this->addSql('CREATE TABLE wood (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, wood VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE wood_item (wood_id INTEGER NOT NULL, item_id INTEGER NOT NULL, PRIMARY KEY(wood_id, item_id))');
        $this->addSql('CREATE INDEX IDX_1187DBFD7B2710BE ON wood_item (wood_id)');
        $this->addSql('CREATE INDEX IDX_1187DBFD126F525E ON wood_item (item_id)');
        $this->addSql('CREATE TABLE order_row (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, order_id_id INTEGER NOT NULL, row_id INTEGER NOT NULL, quantity INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_C76BB9BBFCDAEAAA ON order_row (order_id_id)');
        $this->addSql('CREATE TABLE item (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, order_row_id_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, img VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, width NUMERIC(5, 2) NOT NULL, height NUMERIC(10, 0) NOT NULL, item_amount INTEGER NOT NULL, item_price NUMERIC(10, 0) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_1F1B251EE0610BDF ON item (order_row_id_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('sqlite' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE "order"');
        $this->addSql('DROP TABLE item_category');
        $this->addSql('DROP TABLE item_category_item');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE wood');
        $this->addSql('DROP TABLE wood_item');
        $this->addSql('DROP TABLE order_row');
        $this->addSql('DROP TABLE item');
    }
}
