<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190531160900 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('sqlite' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE image (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, source VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE image_item (image_id INTEGER NOT NULL, item_id INTEGER NOT NULL, PRIMARY KEY(image_id, item_id))');
        $this->addSql('CREATE INDEX IDX_ED21CF803DA5256D ON image_item (image_id)');
        $this->addSql('CREATE INDEX IDX_ED21CF80126F525E ON image_item (item_id)');
        $this->addSql('CREATE TABLE image_wood (image_id INTEGER NOT NULL, wood_id INTEGER NOT NULL, PRIMARY KEY(image_id, wood_id))');
        $this->addSql('CREATE INDEX IDX_CEA7F3933DA5256D ON image_wood (image_id)');
        $this->addSql('CREATE INDEX IDX_CEA7F3937B2710BE ON image_wood (wood_id)');
        $this->addSql('DROP INDEX IDX_1F1B251EE0610BDF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__item AS SELECT id, order_row_id_id, name, img, description, width, height, item_amount, item_price FROM item');
        $this->addSql('DROP TABLE item');
        $this->addSql('CREATE TABLE item (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, order_row_id_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, img VARCHAR(255) NOT NULL COLLATE BINARY, description VARCHAR(255) NOT NULL COLLATE BINARY, width NUMERIC(5, 2) NOT NULL, height NUMERIC(10, 0) NOT NULL, item_amount INTEGER NOT NULL, item_price NUMERIC(10, 0) NOT NULL, CONSTRAINT FK_1F1B251EE0610BDF FOREIGN KEY (order_row_id_id) REFERENCES order_row (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO item (id, order_row_id_id, name, img, description, width, height, item_amount, item_price) SELECT id, order_row_id_id, name, img, description, width, height, item_amount, item_price FROM __temp__item');
        $this->addSql('DROP TABLE __temp__item');
        $this->addSql('CREATE INDEX IDX_1F1B251EE0610BDF ON item (order_row_id_id)');
        $this->addSql('DROP INDEX IDX_782CF782126F525E');
        $this->addSql('DROP INDEX IDX_782CF782F22EC5D4');
        $this->addSql('CREATE TEMPORARY TABLE __temp__item_category_item AS SELECT item_category_id, item_id FROM item_category_item');
        $this->addSql('DROP TABLE item_category_item');
        $this->addSql('CREATE TABLE item_category_item (item_category_id INTEGER NOT NULL, item_id INTEGER NOT NULL, PRIMARY KEY(item_category_id, item_id), CONSTRAINT FK_782CF782F22EC5D4 FOREIGN KEY (item_category_id) REFERENCES item_category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_782CF782126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO item_category_item (item_category_id, item_id) SELECT item_category_id, item_id FROM __temp__item_category_item');
        $this->addSql('DROP TABLE __temp__item_category_item');
        $this->addSql('CREATE INDEX IDX_782CF782126F525E ON item_category_item (item_id)');
        $this->addSql('CREATE INDEX IDX_782CF782F22EC5D4 ON item_category_item (item_category_id)');
        $this->addSql('DROP INDEX IDX_1187DBFD126F525E');
        $this->addSql('DROP INDEX IDX_1187DBFD7B2710BE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__wood_item AS SELECT wood_id, item_id FROM wood_item');
        $this->addSql('DROP TABLE wood_item');
        $this->addSql('CREATE TABLE wood_item (wood_id INTEGER NOT NULL, item_id INTEGER NOT NULL, PRIMARY KEY(wood_id, item_id), CONSTRAINT FK_1187DBFD7B2710BE FOREIGN KEY (wood_id) REFERENCES wood (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_1187DBFD126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO wood_item (wood_id, item_id) SELECT wood_id, item_id FROM __temp__wood_item');
        $this->addSql('DROP TABLE __temp__wood_item');
        $this->addSql('CREATE INDEX IDX_1187DBFD126F525E ON wood_item (item_id)');
        $this->addSql('CREATE INDEX IDX_1187DBFD7B2710BE ON wood_item (wood_id)');
        $this->addSql('DROP INDEX IDX_C76BB9BBFCDAEAAA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__order_row AS SELECT id, order_id_id, row_id, quantity FROM order_row');
        $this->addSql('DROP TABLE order_row');
        $this->addSql('CREATE TABLE order_row (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, order_id_id INTEGER NOT NULL, row_id INTEGER NOT NULL, quantity INTEGER NOT NULL, CONSTRAINT FK_C76BB9BBFCDAEAAA FOREIGN KEY (order_id_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO order_row (id, order_id_id, row_id, quantity) SELECT id, order_id_id, row_id, quantity FROM __temp__order_row');
        $this->addSql('DROP TABLE __temp__order_row');
        $this->addSql('CREATE INDEX IDX_C76BB9BBFCDAEAAA ON order_row (order_id_id)');
        $this->addSql('DROP INDEX IDX_F52993989D86650F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__order AS SELECT id, user_id_id, sum_price, order_date FROM "order"');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('CREATE TABLE "order" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id_id INTEGER NOT NULL, sum_price NUMERIC(10, 0) NOT NULL, order_date VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_F52993989D86650F FOREIGN KEY (user_id_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO "order" (id, user_id_id, sum_price, order_date) SELECT id, user_id_id, sum_price, order_date FROM __temp__order');
        $this->addSql('DROP TABLE __temp__order');
        $this->addSql('CREATE INDEX IDX_F52993989D86650F ON "order" (user_id_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('sqlite' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE image_item');
        $this->addSql('DROP TABLE image_wood');
        $this->addSql('DROP INDEX IDX_1F1B251EE0610BDF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__item AS SELECT id, order_row_id_id, name, img, description, width, height, item_amount, item_price FROM item');
        $this->addSql('DROP TABLE item');
        $this->addSql('CREATE TABLE item (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, order_row_id_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, img VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, width NUMERIC(5, 2) NOT NULL, height NUMERIC(10, 0) NOT NULL, item_amount INTEGER NOT NULL, item_price NUMERIC(10, 0) NOT NULL)');
        $this->addSql('INSERT INTO item (id, order_row_id_id, name, img, description, width, height, item_amount, item_price) SELECT id, order_row_id_id, name, img, description, width, height, item_amount, item_price FROM __temp__item');
        $this->addSql('DROP TABLE __temp__item');
        $this->addSql('CREATE INDEX IDX_1F1B251EE0610BDF ON item (order_row_id_id)');
        $this->addSql('DROP INDEX IDX_782CF782F22EC5D4');
        $this->addSql('DROP INDEX IDX_782CF782126F525E');
        $this->addSql('CREATE TEMPORARY TABLE __temp__item_category_item AS SELECT item_category_id, item_id FROM item_category_item');
        $this->addSql('DROP TABLE item_category_item');
        $this->addSql('CREATE TABLE item_category_item (item_category_id INTEGER NOT NULL, item_id INTEGER NOT NULL, PRIMARY KEY(item_category_id, item_id))');
        $this->addSql('INSERT INTO item_category_item (item_category_id, item_id) SELECT item_category_id, item_id FROM __temp__item_category_item');
        $this->addSql('DROP TABLE __temp__item_category_item');
        $this->addSql('CREATE INDEX IDX_782CF782F22EC5D4 ON item_category_item (item_category_id)');
        $this->addSql('CREATE INDEX IDX_782CF782126F525E ON item_category_item (item_id)');
        $this->addSql('DROP INDEX IDX_F52993989D86650F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__order AS SELECT id, user_id_id, sum_price, order_date FROM "order"');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('CREATE TABLE "order" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id_id INTEGER NOT NULL, sum_price NUMERIC(10, 0) NOT NULL, order_date VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO "order" (id, user_id_id, sum_price, order_date) SELECT id, user_id_id, sum_price, order_date FROM __temp__order');
        $this->addSql('DROP TABLE __temp__order');
        $this->addSql('CREATE INDEX IDX_F52993989D86650F ON "order" (user_id_id)');
        $this->addSql('DROP INDEX IDX_C76BB9BBFCDAEAAA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__order_row AS SELECT id, order_id_id, row_id, quantity FROM order_row');
        $this->addSql('DROP TABLE order_row');
        $this->addSql('CREATE TABLE order_row (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, order_id_id INTEGER NOT NULL, row_id INTEGER NOT NULL, quantity INTEGER NOT NULL)');
        $this->addSql('INSERT INTO order_row (id, order_id_id, row_id, quantity) SELECT id, order_id_id, row_id, quantity FROM __temp__order_row');
        $this->addSql('DROP TABLE __temp__order_row');
        $this->addSql('CREATE INDEX IDX_C76BB9BBFCDAEAAA ON order_row (order_id_id)');
        $this->addSql('DROP INDEX IDX_1187DBFD7B2710BE');
        $this->addSql('DROP INDEX IDX_1187DBFD126F525E');
        $this->addSql('CREATE TEMPORARY TABLE __temp__wood_item AS SELECT wood_id, item_id FROM wood_item');
        $this->addSql('DROP TABLE wood_item');
        $this->addSql('CREATE TABLE wood_item (wood_id INTEGER NOT NULL, item_id INTEGER NOT NULL, PRIMARY KEY(wood_id, item_id))');
        $this->addSql('INSERT INTO wood_item (wood_id, item_id) SELECT wood_id, item_id FROM __temp__wood_item');
        $this->addSql('DROP TABLE __temp__wood_item');
        $this->addSql('CREATE INDEX IDX_1187DBFD7B2710BE ON wood_item (wood_id)');
        $this->addSql('CREATE INDEX IDX_1187DBFD126F525E ON wood_item (item_id)');
    }
}
