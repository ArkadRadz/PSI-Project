<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 */
class Orders
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="decimal", precision=10, scale=0) */
    private $Sum_price;

    /** @ORM\Column(type="string", length=255) */
    private $Order_date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="Order_ID")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User_ID;

    /** @ORM\OneToMany(targetEntity="App\Entity\OrderRow", mappedBy="Order_ID", orphanRemoval=true) */
    private $Order_Row_ID;

    /** @ORM\Column(type="boolean") */
    private $isFinished = false;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $delivery_street;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $delivery_postcode;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $delivery_country;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $delivery_city;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalQuantity = 0;

    public function __construct()
    {
        $this->Order_Row_ID = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSumPrice()
    {
        return $this->Sum_price;
    }

    public function setSumPrice($Sum_price): self
    {
        $this->Sum_price = $Sum_price;

        return $this;
    }

    public function getOrderDate(): ?string
    {
        return $this->Order_date;
    }

    public function setOrderDate(string $Order_date): self
    {
        $this->Order_date = $Order_date;

        return $this;
    }

    public function getUserID(): ?User
    {
        return $this->User_ID;
    }

    public function setUserID(?User $User_ID): self
    {
        $this->User_ID = $User_ID;

        return $this;
    }

    /**
     * @return Collection|OrderRow[]
     */
    public function getOrderRowID(): Collection
    {
        return $this->Order_Row_ID;
    }

    public function addOrderRowID(OrderRow $orderRowID): self
    {
        if (!$this->Order_Row_ID->contains($orderRowID)) {
            $this->Order_Row_ID[] = $orderRowID;
            $orderRowID->setOrderID($this);
        }

        return $this;
    }

    public function removeOrderRowID(OrderRow $orderRowID): self
    {
        if ($this->Order_Row_ID->contains($orderRowID)) {
            $this->Order_Row_ID->removeElement($orderRowID);
            // set the owning side to null (unless already changed)
            if ($orderRowID->getOrderID() === $this) {
                $orderRowID->setOrderID(null);
            }
        }

        return $this;
    }

    public function getIsFinished(): ?bool
    {
        return $this->isFinished;
    }

    public function setIsFinished(bool $isFinished): self
    {
        $this->isFinished = $isFinished;

        return $this;
    }

    public function getDeliveryStreet(): ?string
    {
        return $this->delivery_street;
    }

    public function setDeliveryStreet(?string $delivery_street): self
    {
        $this->delivery_street = $delivery_street;

        return $this;
    }

    public function getDeliveryPostcode(): ?string
    {
        return $this->delivery_postcode;
    }

    public function setDeliveryPostcode(?string $delivery_postcode): self
    {
        $this->delivery_postcode = $delivery_postcode;

        return $this;
    }

    public function getDeliveryCountry(): ?string
    {
        return $this->delivery_country;
    }

    public function setDeliveryCountry(?string $delivery_country): self
    {
        $this->delivery_country = $delivery_country;

        return $this;
    }

    public function getDeliveryCity(): ?string
    {
        return $this->delivery_city;
    }

    public function setDeliveryCity(?string $delivery_city): self
    {
        $this->delivery_city = $delivery_city;

        return $this;
    }

    public function getTotalQuantity(): ?int
    {
        return $this->totalQuantity;
    }

    public function setTotalQuantity(?int $totalQuantity): self
    {
        $this->totalQuantity = $totalQuantity;

        return $this;
    }
}
