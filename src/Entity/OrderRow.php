<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRowRepository")
 */
class OrderRow
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="integer") */
    private $Quantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Orders", inversedBy="Order_Row_ID")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Order_ID;

    /** @ORM\OneToMany(targetEntity="App\Entity\Item", mappedBy="Order_Row_ID") */
    private $Item_ID;

    public function __construct()
    {
        $this->Item_ID = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->Quantity;
    }

    public function setQuantity(int $Quantity): self
    {
        $this->Quantity = $Quantity;

        return $this;
    }

    public function getOrderID(): ?Orders
    {
        return $this->Order_ID;
    }

    public function setOrderID(?Orders $Order_ID): self
    {
        $this->Order_ID = $Order_ID;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItemID(): Collection
    {
        return $this->Item_ID;
    }

    public function addItemID(Item $itemID): self
    {
        if (!$this->Item_ID->contains($itemID)) {
            $this->Item_ID[] = $itemID;
            $itemID->setOrderRowID($this);
        }

        return $this;
    }

    public function removeItemID(Item $itemID): self
    {
        if ($this->Item_ID->contains($itemID)) {
            $this->Item_ID->removeElement($itemID);
            // set the owning side to null (unless already changed)
            if ($itemID->getOrderRowID() === $this) {
                $itemID->setOrderRowID(null);
            }
        }

        return $this;
    }
}
