<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="string", length=255) */
    private $source;

    /** @ORM\ManyToMany(targetEntity="App\Entity\Item", inversedBy="images") */
    private $Item;

    /** @ORM\ManyToMany(targetEntity="App\Entity\Wood", inversedBy="images") */
    private $Wood;

    public function __construct()
    {
        $this->Item = new ArrayCollection();
        $this->Wood = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItem(): Collection
    {
        return $this->Item;
    }

    public function addItem(Item $item): self
    {
        if (!$this->Item->contains($item)) {
            $this->Item[] = $item;
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->Item->contains($item)) {
            $this->Item->removeElement($item);
        }

        return $this;
    }

    /**
     * @return Collection|Wood[]
     */
    public function getWood(): Collection
    {
        return $this->Wood;
    }

    public function addWood(Wood $wood): self
    {
        if (!$this->Wood->contains($wood)) {
            $this->Wood[] = $wood;
        }

        return $this;
    }

    public function removeWood(Wood $wood): self
    {
        if ($this->Wood->contains($wood)) {
            $this->Wood->removeElement($wood);
        }

        return $this;
    }
}
