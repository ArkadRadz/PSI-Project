<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"login"}, message="There is already an account with this login")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="string", length=180, unique=true) */
    private $uuid;

    /** @ORM\Column(type="json") */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /** @ORM\Column(type="string", length=255) */
    private $login;

    /** @ORM\Column(type="string", length=255) */
    private $email;

    /** @ORM\Column(type="string", length=255) */
    private $NIP;

    /** @ORM\Column(type="boolean") */
    private $Company_Account;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $Company_Name;

    /** @ORM\OneToMany(targetEntity="App\Entity\Orders", mappedBy="User_ID") */
    private $Order_ID;

    /** @ORM\OneToMany(targetEntity="App\Entity\Wishlist", mappedBy="user", orphanRemoval=true) */
    private $wishlists;

    public function __construct()
    {
        $this->Order_ID = new ArrayCollection();
        $this->wishlists = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->uuid;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNIP(): ?string
    {
        return $this->NIP;
    }

    public function setNIP(string $NIP): self
    {
        $this->NIP = $NIP;

        return $this;
    }

    public function getCompanyAccount(): ?bool
    {
        return $this->Company_Account;
    }

    public function setCompanyAccount(bool $Company_Account): self
    {
        $this->Company_Account = $Company_Account;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->Company_Name;
    }

    public function setCompanyName(?string $Company_Name): self
    {
        $this->Company_Name = $Company_Name;

        return $this;
    }

    /**
     * @return Collection|Orders[]
     */
    public function getOrderID(): Collection
    {
        return $this->Order_ID;
    }

    public function addOrderID(Orders $orderID): self
    {
        if (!$this->Order_ID->contains($orderID)) {
            $this->Order_ID[] = $orderID;
            $orderID->setUserID($this);
        }

        return $this;
    }

    public function removeOrderID(Orders $orderID): self
    {
        if ($this->Order_ID->contains($orderID)) {
            $this->Order_ID->removeElement($orderID);
            // set the owning side to null (unless already changed)
            if ($orderID->getUserID() === $this) {
                $orderID->setUserID(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Wishlist[]
     */
    public function getWishlists(): Collection
    {
        return $this->wishlists;
    }

    public function addWishlist(Wishlist $wishlist): self
    {
        if (!$this->wishlists->contains($wishlist)) {
            $this->wishlists[] = $wishlist;
            $wishlist->setUser($this);
        }

        return $this;
    }

    public function removeWishlist(Wishlist $wishlist): self
    {
        if ($this->wishlists->contains($wishlist)) {
            $this->wishlists->removeElement($wishlist);
            // set the owning side to null (unless already changed)
            if ($wishlist->getUser() === $this) {
                $wishlist->setUser(null);
            }
        }

        return $this;
    }
}
