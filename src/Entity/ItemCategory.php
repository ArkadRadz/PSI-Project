<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemCategoryRepository")
 */
class ItemCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="string", length=255) */
    private $Category;

    /** @ORM\ManyToMany(targetEntity="App\Entity\Item", inversedBy="Item_Category_ID") */
    private $Item_ID;

    public function __construct()
    {
        $this->Item_ID = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getCategory();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?string
    {
        return $this->Category;
    }

    public function setCategory(string $Category): self
    {
        $this->Category = $Category;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItemID(): Collection
    {
        return $this->Item_ID;
    }

    public function addItemID(Item $itemID): self
    {
        if (!$this->Item_ID->contains($itemID)) {
            $this->Item_ID[] = $itemID;
        }

        return $this;
    }

    public function removeItemID(Item $itemID): self
    {
        if ($this->Item_ID->contains($itemID)) {
            $this->Item_ID->removeElement($itemID);
        }

        return $this;
    }
}
