<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="string", length=255) */
    private $name;

    /** @ORM\Column(type="string", length=255) */
    private $description;

    /** @ORM\Column(type="decimal", precision=5, scale=2) */
    private $width;

    /** @ORM\Column(type="decimal", precision=10, scale=0) */
    private $height;

    /** @ORM\Column(type="integer") */
    private $item_amount;

    /** @ORM\Column(type="decimal", precision=10, scale=0) */
    private $item_price;

    /** @ORM\ManyToOne(targetEntity="App\Entity\OrderRow", inversedBy="Item_ID") */
    private $Order_Row_ID;

    /** @ORM\ManyToMany(targetEntity="App\Entity\ItemCategory", mappedBy="Item_ID") */
    private $Item_Category_ID;

    /** @ORM\ManyToMany(targetEntity="App\Entity\Wood", mappedBy="Item_ID") */
    private $Wood_ID;

    /** @ORM\ManyToMany(targetEntity="App\Entity\Image", mappedBy="Item") */
    private $images;

    /** @ORM\OneToMany(targetEntity="App\Entity\Wishlist", mappedBy="item", orphanRemoval=true) */
    private $wishlists;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $length;

    public function __construct()
    {
        $this->Item_Category_ID = new ArrayCollection();
        $this->Wood_ID = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->wishlists = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getItemAmount(): ?int
    {
        return $this->item_amount;
    }

    public function setItemAmount(int $item_amount): self
    {
        $this->item_amount = $item_amount;

        return $this;
    }

    public function getItemPrice()
    {
        return $this->item_price;
    }

    public function setItemPrice($item_price): self
    {
        $this->item_price = $item_price;

        return $this;
    }

    public function getOrderRowID(): ?OrderRow
    {
        return $this->Order_Row_ID;
    }

    public function setOrderRowID(?OrderRow $Order_Row_ID): self
    {
        $this->Order_Row_ID = $Order_Row_ID;

        return $this;
    }

    /**
     * @return Collection|ItemCategory[]
     */
    public function getItemCategoryID(): Collection
    {
        return $this->Item_Category_ID;
    }

    public function addItemCategoryID(ItemCategory $itemCategoryID): self
    {
        if (!$this->Item_Category_ID->contains($itemCategoryID)) {
            $this->Item_Category_ID[] = $itemCategoryID;
            $itemCategoryID->addItemID($this);
        }

        return $this;
    }

    public function removeItemCategoryID(ItemCategory $itemCategoryID): self
    {
        if ($this->Item_Category_ID->contains($itemCategoryID)) {
            $this->Item_Category_ID->removeElement($itemCategoryID);
            $itemCategoryID->removeItemID($this);
        }

        return $this;
    }

    /**
     * @return Collection|Wood[]
     */
    public function getWoodID(): Collection
    {
        return $this->Wood_ID;
    }

    public function addWoodID(Wood $woodID): self
    {
        if (!$this->Wood_ID->contains($woodID)) {
            $this->Wood_ID[] = $woodID;
            $woodID->addItemID($this);
        }

        return $this;
    }

    public function removeWoodID(Wood $woodID): self
    {
        if ($this->Wood_ID->contains($woodID)) {
            $this->Wood_ID->removeElement($woodID);
            $woodID->removeItemID($this);
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->addItem($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            $image->removeItem($this);
        }

        return $this;
    }

    /**
     * @return Collection|Wishlist[]
     */
    public function getWishlists(): Collection
    {
        return $this->wishlists;
    }

    public function addWishlist(Wishlist $wishlist): self
    {
        if (!$this->wishlists->contains($wishlist)) {
            $this->wishlists[] = $wishlist;
            $wishlist->setItem($this);
        }

        return $this;
    }

    public function removeWishlist(Wishlist $wishlist): self
    {
        if ($this->wishlists->contains($wishlist)) {
            $this->wishlists->removeElement($wishlist);
            // set the owning side to null (unless already changed)
            if ($wishlist->getItem() === $this) {
                $wishlist->setItem(null);
            }
        }

        return $this;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setLength($length): self
    {
        $this->length = $length;

        return $this;
    }
}
