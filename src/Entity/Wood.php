<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WoodRepository")
 */
class Wood
{
    public function __toString()
    {
        return $this->getWood();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="string", length=255) */
    private $Wood;

    /** @ORM\ManyToMany(targetEntity="App\Entity\Item", inversedBy="Wood_ID") */
    private $Item_ID;

    /** @ORM\ManyToMany(targetEntity="App\Entity\Image", mappedBy="Wood") */
    private $images;

    public function __construct()
    {
        $this->Item_ID = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWood(): ?string
    {
        return $this->Wood;
    }

    public function setWood(string $Wood): self
    {
        $this->Wood = $Wood;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItemID(): Collection
    {
        return $this->Item_ID;
    }

    public function addItemID(Item $itemID): self
    {
        if (!$this->Item_ID->contains($itemID)) {
            $this->Item_ID[] = $itemID;
        }

        return $this;
    }

    public function removeItemID(Item $itemID): self
    {
        if ($this->Item_ID->contains($itemID)) {
            $this->Item_ID->removeElement($itemID);
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->addWood($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            $image->removeWood($this);
        }

        return $this;
    }
}
