<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUuid('1');
        $password = password_hash('admin', \PASSWORD_BCRYPT);
        $user->setPassword($password);
        $user->setEmail('admin@admin.com');
        $user->setLogin('admin');
        $user->setCompanyAccount(false);
        $user->setNIP('admin');
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);
        $manager->flush();
    }
}
