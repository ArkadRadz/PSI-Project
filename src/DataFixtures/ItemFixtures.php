<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Image;
use App\Entity\Item;
use App\Entity\ItemCategory;
use App\Entity\Wood;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Finder\Finder;

class ItemFixtures extends Fixture
{
    private function getItemsIDFromCategory($category_ID)
    {
        $id = intval($category_ID);

        switch ($id) {
            case 1:
                return range(1, 33);
            case 2:
                return range(33, 36);
            case 3:
                return range(37, 41);
            case 5:
                return array_merge(range(48, 51), range(58, 60));
            case 4:
                return array_merge(array(29), range(43, 47));
            case 6:
                return range(52, 57);
            case 7:
                return range(61, 78);
            default:
                return array();
        }
    }

    public function load(ObjectManager $manager)
    {
        $imageFinder = new Finder();
        $imageFinder->files()->in(__DIR__.'/images');
        $images = [];
        foreach ($imageFinder as $file) {
            $images[] = $file;
        }
        ;
        $finder = new Finder();
        $finder->files()->in(__DIR__);
        if ($finder->hasResults()) {  
            foreach ($finder as $file) {
                if ($file->getExtension() == 'txt') {
                    if ($file->getFilenameWithoutExtension() == 'woods') {
                        $openedFile = file($file->getPathname(), FILE_IGNORE_NEW_LINES);
                        foreach ($openedFile as $woodType) {
                            $wood = new Wood();
                            $wood->setWood($woodType);
                            $manager->persist($wood);
                            $manager->flush();
                        }
                    }
                }
            }
  
            foreach ($finder as $file) {
                if ($file->getExtension() == 'json') {
                    $openedFile = file_get_contents($file->getPathname());
                    $json = json_decode($openedFile);
                    $woods = $manager->getRepository(Wood::class)->findAll();
                    foreach ($json as $json_item) {
                        $item = new Item();
                        $item->setName($json_item->{'name'});
                        $item->setDescription($json_item->{'description'});
                        $item->setWidth($json_item->{'width'});
                        $item->setHeight($json_item->{'height'});
                        $item->setLength($json_item->{'length'});
                        $item->setItemAmount($json_item->{'item_amount'});
                        $item->setItemPrice($json_item->{'item_price'});
                        $item->addWoodID($woods[rand(0, count($woods)-1)]);
                        $image = new Image();
                        $image->setSource($images[rand(0, count($images)-1)]->getFilename());
                        $item->addImage($image);
                        $manager->persist($image);
                        $manager->persist($item);
                        $manager->flush();
                    }
                }
            }

            foreach ($finder as $file) {
                if ($file->getExtension() == 'txt') {    
                    if ($file->getFilenameWithoutExtension() == 'categories') {
                        $openedFile = file($file->getPathname(), FILE_IGNORE_NEW_LINES);
                        $category_id = 1;
                        foreach ($openedFile as $itemCategoryName) {
                            $itemCategory = new ItemCategory();
                            $itemCategory->setCategory($itemCategoryName);

                            $item_IDs = $this->getItemsIDFromCategory($category_id);
                            foreach($item_IDs as $item_ID) {
                                $itemCategory->addItemID(
                                    $manager->getRepository(Item::class)->find($item_ID));
                            }

                            $manager->persist($itemCategory);
                            $manager->flush();
                            $category_id++;
                        }
                    }

                }
            }
        }
    }
}
