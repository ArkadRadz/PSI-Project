<?php

declare(strict_types=1);

namespace App\Controller\Rest\Wishlist;

use App\Entity\Item;
use App\Entity\User;
use App\Entity\Wishlist;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class WishlistController extends AbstractController
{
    /**
     * @Rest\Post("/add_to_wishlist", name="add_to_wishlist")
     */
    public function postItem(Request $request): View
    {
        $em = $this->getDoctrine()->getManager();

        $userID = $request->get('user_id');
        $itemID = $request->get('item_id');

        if (!$userID || !$itemID) {
            return View::create('not enough data', 404);
        }

        $user = $em->getRepository(User::class)->find($userID);

        if (!$user) {
            return View::create('user not found', 404);
        }

        $item = $em->getRepository(Item::class)->find($itemID);

        if (!$item) {
            return View::create('item not found', 404);
        }

        $wishlist = $em->getRepository(Wishlist::class)->createQueryBuilder('wishlist')
            ->where('wishlist.item = :item')
            ->andWhere('wishlist.user = :user')
            ->setParameter('item', $item)
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if ($wishlist) {
            return View::create('item is already in wishlist', 404);
        }

        $wishlist = new Wishlist();

        $wishlist->setItem($item);
        $wishlist->setUser($user);
        $em->persist($wishlist);
        $em->flush();

        return View::create('ok', 201);
    }

    /**
     * @Rest\Delete("/remove_from_wishlist", name="remove_from_wishlist")
     */
    public function removeItem(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $userID = intval($request->get('user_id'));
        $itemID = intval($request->get('item_id'));

        if (!$userID || !$itemID) {
            return View::create('not enough data', 404);
        }

        $user = $em->getRepository(User::class)->find($userID);

        if (!$user) {
            return View::create('user not found', 404);
        }

        $item = $em->getRepository(Item::class)->find($itemID);

        if (!$item) {
            return View::create('item not found', 404);
        }

        $wishlist = $em->getRepository(Wishlist::class)->createQueryBuilder('wishlist')
            ->where('wishlist.item = :item')
            ->andWhere('wishlist.user = :user')
            ->setParameter('item', $item)
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (!$wishlist) {
            return View::create('could not find wishlist', 404);
        }

        $em->remove($wishlist);
        $em->flush();

        return View::create('ok', 201);
    }
}
