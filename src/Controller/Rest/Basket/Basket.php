<?php

declare(strict_types=1);

namespace App\Controller\Rest\Basket;

use App\Entity\Item;
use App\Entity\OrderRow;
use App\Entity\Orders;
use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class Basket extends AbstractController
{
    /**
     * @Rest\Post("/add_to_basket", name="add_to_basket")
     */
    public function addToBasket(Request $request): View
    {
        $em = $this->getDoctrine()->getManager();
        $userID = $request->get('user_id');
        $productID = $request->get('product_id');
        $productQuantity = intval($request->get('product_quantity'));
        $users = $em->getRepository(User::class);
        $products = $em->getRepository(Item::class);

        $user = $users->findOneBy(['id' => $userID]);
        $product = $products->findOneBy(['id' => $productID]);

        if (!$user || !$product) {
            return View::create('not found', 404);
        }

        $userOrders = $em->getRepository(Orders::class)->createQueryBuilder('userOrder')
            ->where('userOrder.isFinished = false')
            ->andWhere('userOrder.User_ID = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->execute()
        ;


        if (!$userOrders) {
            /** @var Orders $order */
            $order = new Orders();

            /** @var OrderRow $orderRow */
            $orderRow = new OrderRow();

            $orderRow->addItemID($product);
            $orderRow->setQuantity($productQuantity);

            $order->setTotalQuantity($productQuantity);
            $order->setSumPrice($productQuantity * $product->getItemPrice());
            $order->setUserID($user);
            $order->setOrderDate(date('d/m/y H:i:s'));
            $order->addOrderRowID($orderRow);
            $em->persist($orderRow);
            $em->persist($order);
            $em->flush();

            return View::create($order->getTotalQuantity(), 201);
        }

        /** @var Orders|null $userOrder */
        $userOrder = $em->getRepository(Orders::class)->createQueryBuilder('userOrder')
            ->leftJoin('userOrder.Order_Row_ID', 'orderRow')
            ->where('userOrder.isFinished = false')
            ->andWhere('orderRow.Order_ID = userOrder.id')
            ->andWhere('userOrder.User_ID = :user')
            ->setParameter('user', $user)
            ->orderBy('userOrder.id', 'DESC')
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if ($userOrder) {
            /** @var OrderRow|null $orderRow */
            $orderRow = $em->getRepository(OrderRow::class)->createQueryBuilder('orderRow')
                ->leftJoin('orderRow.Order_ID', 'userOrder')
                ->leftJoin('orderRow.Item_ID', 'product')
                ->where('orderRow.Order_ID = userOrder')
                ->andWhere('product.id = :item')
                ->setParameter('item', $product->getId())
                ->getQuery()
                ->getOneOrNullResult()
            ;

            if (!$orderRow) {
                /** @var OrderRow $orderRow */
                $orderRow = new OrderRow();

                $orderRow->addItemID($product);
                $orderRow->setQuantity($productQuantity);

                $userOrder->addOrderRowID($orderRow);

                $totalQuantity = $userOrder->getTotalQuantity() + $productQuantity;

                $userOrder->setTotalQuantity($totalQuantity);
                $newPrice = $userOrder->getSumPrice() + $product->getItemPrice() * $productQuantity;
                $userOrder->setSumPrice($newPrice);
                $em->persist($orderRow);
                $em->persist($userOrder);
                $em->flush();

                return View::create($totalQuantity, 201);
            }

            $newQuantity = $orderRow->getQuantity() + $productQuantity;
            $orderRow->setQuantity($newQuantity);

            $em->persist($orderRow);

            $newTotalQuantity = $userOrder->getTotalQuantity() + $productQuantity;
            $newSumPrice = $userOrder->getSumPrice() + $product->getItemPrice() * $productQuantity;

            $userOrder->setSumPrice($newSumPrice);
            $userOrder->setTotalQuantity($newTotalQuantity);

            $em->persist($userOrder);
            $em->flush();

            return View::create($newTotalQuantity, 201);
        } else {
            $foundOrder = $em->getRepository(Orders::class)->createQueryBuilder('userOrder')
                ->where('userOrder.isFinished = false')
                ->andWhere('userOrder.User_ID = :user')
                ->setParameter('user', $user)
                ->orderBy('userOrder.id', 'DESC')
                ->getQuery()
                ->getOneOrNullResult()
            ;

            /** @var OrderRow $orderRow */
            $orderRow = new OrderRow();

            $orderRow->addItemID($product);
            $orderRow->setQuantity($productQuantity);

            $foundOrder->addOrderRowID($orderRow);

            $totalQuantity = $foundOrder->getTotalQuantity() + $productQuantity;

            $foundOrder->setTotalQuantity($totalQuantity);
            $newPrice = $foundOrder->getSumPrice() + $product->getItemPrice();
            $foundOrder->setSumPrice($newPrice);
            $em->persist($orderRow);
            $em->persist($foundOrder);
            $em->flush();

            return View::create($totalQuantity, 201);
        }

        return View::create('nothin', 201);
    }
}
