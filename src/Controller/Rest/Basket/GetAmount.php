<?php

declare(strict_types=1);

namespace App\Controller\Rest\Basket;

use App\Entity\Orders;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class GetAmount extends AbstractController
{
    /**
     * @Rest\Get("/get_order_amount/{order_id}")
     */
    public function getAmount(Request $request, int $order_id): View
    {
        if (!$order_id) {
            return View::create('not enough data', 404);
        }

        $em = $this->getDoctrine()->getManager();

        $order = $em->getRepository(Orders::class)->find($order_id);

        if (!$order) {
            return View::create('not found', 404);
        }

        return View::create($order->getSumPrice(), 201);
    }
}
