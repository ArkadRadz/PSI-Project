<?php

declare(strict_types=1);

namespace App\Controller\Rest\Item;

use App\Entity\Image;
use App\Entity\Item;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DeleteImageFromItem extends AbstractController
{
    /**
     * @Rest\Delete("/remove_image_from_item")
     */
    public function deleteItem(Request $request): View
    {
        $em = $this->getDoctrine()->getManager();

        $itemID = $request->get('item_id');
        $imageID = $request->get('image_id');

        if (!$itemID || !$imageID) {
            return View::create('not enough data', 404);
        }

        $item = $em->getRepository(Item::class)->find($itemID);
        $image = $em->getRepository(Image::class)->find($imageID);

        if (!$item || !$image) {
            return View::create('not found', 404);
        }

        if (!$item->getImages()->contains($image)) {
            return View::create('image not found in item', 404);
        }

        $item->getImages()->removeElement($item);
        $em->remove($image);
        $em->persist($item);
        $em->flush();

        return View::create('ok', 201);
    }
}
