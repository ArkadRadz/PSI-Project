<?php

declare(strict_types=1);

namespace App\Controller\Rest\Item;

use App\Entity\Item;
use App\Entity\OrderRow;
use App\Entity\Orders;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DeleteItemFromBasket extends AbstractController
{
    /**
     * @Rest\Delete("/remove_item_from_basket")
     */
    public function deleteItem(Request $request): View
    {
        $em = $this->getDoctrine()->getManager();

        $basketID = $request->get('basket_id');
        $itemID = $request->get('product_id');

        if (!$basketID || !$itemID) {
            return View::create('Not enough data', 404);
        }

        $basket = $em->getRepository(Orders::class)->find($basketID);

        if (!$basket) {
            return View::create('Not found', 404);
        }

        $item = $em->getRepository(Item::class)->find($itemID);
        if (!$item) {
            return View::create('Not found', 404);
        }

        /** @var OrderRow $orderRowWithItem */
        $orderRowWithItem = $em->getRepository(OrderRow::class)->createQueryBuilder('orderrow')
            ->leftJoin('orderrow.Order_ID', 'userOrder')
            ->leftJoin('orderrow.Item_ID', 'itemId')
            ->where('orderrow.Order_ID = :orderID')
            ->andWhere('userOrder.isFinished = false')
            ->andWhere('itemId.id = :item_id')
            ->setParameter('orderID', $basketID)
            ->setParameter('item_id', $itemID)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (!$orderRowWithItem->getItemID($item)) {
            return View::create('Not found', 404);
        }

        $order = $orderRowWithItem->getOrderID();
        $currentPrice = $order->getSumPrice();

        /** @var Item $item */
        $item = $orderRowWithItem->getItemID()->first();

        $itemPrice = $item->getItemPrice();
        $itemQuantity = $orderRowWithItem->getQuantity();

        $newPrice = $currentPrice - $itemPrice * $itemQuantity;

        $order->setSumPrice($newPrice);
        $newQuantity = $order->getTotalQuantity() - $itemQuantity;
        $order->setTotalQuantity($newQuantity);
        $orderRowWithItem->removeItemID($item);

        if ($orderRowWithItem->getItemID()->isEmpty()) {
            $em->remove($orderRowWithItem);
        } else {
            $em->persist($orderRowWithItem);
        }
        $em->flush();

        return View::create('ok', 201);
    }

    /**
     * @Rest\Delete("/remove_one_item_from_basket")
     */
    public function deleteOneItem(Request $request): View
    {
        $em = $this->getDoctrine()->getManager();

        $basketID = $request->get('basket_id');
        $itemID = $request->get('product_id');

        if (!$basketID || !$itemID) {
            return View::create('Not enough data', 404);
        }

        $basket = $em->getRepository(Orders::class)->find($basketID);

        if (!$basket) {
            return View::create('Not found', 404);
        }

        $item = $em->getRepository(Item::class)->find($itemID);

        if (!$item) {
            return View::create('Not found', 404);
        }

        /** @var OrderRow $orderRowWithItem */
        $orderRowWithItem = $em->getRepository(OrderRow::class)->createQueryBuilder('orderrow')
            ->leftJoin('orderrow.Order_ID', 'userOrder')
            ->leftJoin('orderrow.Item_ID', 'itemId')
            ->where('orderrow.Order_ID = :orderID')
            ->andWhere('userOrder.isFinished = false')
            ->andWhere('itemId.id = :item_id')
            ->setParameter('orderID', $basketID)
            ->setParameter('item_id', $itemID)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (!$orderRowWithItem) {
            return View::create('Order row not found', 404);
        }

        if (!$orderRowWithItem->getItemID($item)) {
            return View::create('Not found item in order', 404);
        }

        $order = $orderRowWithItem->getOrderID();
        $currentPrice = $order->getSumPrice();

        /** @var Item $item */
        $item = $orderRowWithItem->getItemID()->first();

        $itemPrice = $item->getItemPrice();
        $itemQuantity = $orderRowWithItem->getQuantity() - 1;
        $orderRowWithItem->setQuantity($itemQuantity);

        $newPrice = $currentPrice - $itemPrice;

        $order->setSumPrice($newPrice);
        $newQuantity = $order->getTotalQuantity() - 1;
        $order->setTotalQuantity($newQuantity);

        if ($orderRowWithItem->getItemID()->isEmpty() || $orderRowWithItem->getQuantity() <= 0) {
            $orderRowWithItem->removeItemID($item);
            $em->remove($orderRowWithItem);

            if ($order->getOrderRowID()->isEmpty()) {
                $em->remove($order);
            }
        } else {
            $em->persist($orderRowWithItem);
        }

        $em->flush();

        return View::create($order->getTotalQuantity(), 201);
    }
}
