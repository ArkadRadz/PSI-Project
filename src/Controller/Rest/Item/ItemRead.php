<?php

declare(strict_types=1);

namespace App\Controller\Rest\Item;

use App\Entity\Item;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ItemRead extends AbstractController
{
    /**
     * @Rest\Get("/item/{itemID}")
     */
    public function getItem(int $itemID): View
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository(Item::class)->find($itemID);
        if ($item) {
            return View::create($item, 201);
        }

        return View::create('Not found', 404);
    }

    public function readItems(Request $request)
    {
    }
}
