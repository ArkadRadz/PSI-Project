<?php

declare(strict_types=1);

namespace App\Controller\Rest\Item;

use App\Entity\Item;
use App\Entity\OrderRow;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ItemCreate extends AbstractController
{
    /**
     * @Rest\Post("/item")
     */
    public function postItem(Request $request): View
    {
        $item = new Item();
        $em = $this->getDoctrine()->getManager();

        $item->setDescription($request->get('description'));
        $item->setHeight('height');
        $item->setImg('img');
        $item->setItemAmount('item_amount');
        $item->setItemPrice('item_price');
        $item->setName('name');
        $orderRowRepository = $em->getRepository(OrderRow::class);

        /** @var OrderRow $orderRow */
        $orderRow = $orderRowRepository->find($request->get('order_row'));
        $item->setOrderRowID($orderRow);
        $item->setWidth('width');

        $em->persist($item);
        $em->flush();

        return View::create($item, 201);
    }
}
