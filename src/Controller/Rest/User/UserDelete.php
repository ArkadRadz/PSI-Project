<?php

declare(strict_types=1);

namespace App\Controller\Rest\User;

use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserDelete extends AbstractController
{
    /**
     * @Rest\Delete("/user/{uuid}", requirements={"uuid"="\d+"})
     */
    public function getItem(int $uuid): View
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneBy(['uuid' => $uuid]);

        if (null != $user) {
            $em->remove($user);
            $em->flush();

            return View::create('removed', 200);
        }

        return View::create('Not found', 404);
    }
}
