<?php

declare(strict_types=1);

namespace App\Controller\Rest\User;

use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserRead extends AbstractController
{
    /**
     * @Rest\Get("/user/{uuid}", requirements={"uuid"="\d+"})
     */
    public function getItem(int $uuid): View
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneBy(['uuid' => $uuid]);

        if ($user) {
            $userArray = [
                'login' => $user->getLogin(),
                'company_account' => $user->getCompanyAccount(),
                'uuid' => $user->getUuid(),
                'email' => $user->getEmail(),
                'nip' => $user->getNIP(),
            ];

            return View::create(json_encode($userArray, \JSON_PRETTY_PRINT), 200);
        }

        return View::create('Not found', 404);
    }
}
