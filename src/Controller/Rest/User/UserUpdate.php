<?php

declare(strict_types=1);

namespace App\Controller\Rest\User;

use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserUpdate extends AbstractController
{
    /**
     * @Rest\Put("/user/{uuid}", requirements={"uuid"="\d+"})
     */
    public function getItem(Request $request, int $uuid): View
    {
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneBy(['uuid' => $uuid]);

        if (null != $user) {
            $user->setLogin($data['login']);
            $user->setEmail($data['email']);
            $password = password_hash($data['password'], \PASSWORD_BCRYPT);
            $user->setPassword($password);
            $user->setCompanyAccount($data['company_account']);
            if (false == $data['company_account']) {
                $user->setCompanyName($data['company_name']);
            }
            if ($data['role']) {
                $user->setRoles(['ROLE_ADMIN']);
            }
            $user->setNIP($data['nip']);

            return View::create('ok', 200);
        }

        return View::create('Not found', 404);
    }
}
