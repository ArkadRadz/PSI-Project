<?php

declare(strict_types=1);

namespace App\Controller\Rest\User;

use App\Entity\User;
use App\Helper\StringGenerator;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserCreate extends AbstractController
{
    /** @var StringGenerator */
    private $stringGenerator;

    public function __construct(StringGenerator $stringGenerator)
    {
        $this->stringGenerator = $stringGenerator;
    }

    /**
     * @Rest\Post("/user")
     */
    public function postUser(Request $request): View
    {
        $data = json_decode($request->getContent(), true);
        /** @var User $user */
        $user = new User();

        $user->setLogin($data['login']);
        $user->setEmail($data['email']);
        $password = password_hash($data['password'], \PASSWORD_BCRYPT);
        $user->setPassword($password);
        $user->setCompanyAccount($data['company_account']);
        if (false == $data['company_account']) {
            $user->setCompanyName($data['company_name']);
        }
        $user->setNIP($data['nip']);
        $uuid = $this->stringGenerator->generateNumbers(10);
        $user->setUuid($uuid);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return View::create(json_encode($data), 201);
    }
}
