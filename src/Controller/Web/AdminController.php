<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\Orders;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $urls = [];
        $urls[] = ['wood create', $this->generateUrl('wood_create')];
        $urls[] = ['item create', $this->generateUrl('item_create')];
        $urls[] = ['item category create', $this->generateUrl('item_category_create')];
        $urls[] = ['view orders', $this->generateUrl('admin_view_orders')];
        $urls[] = ['view users', $this->generateUrl('admin_view_users')];

        return $this->render('admin/index.html.twig', [
            'urls' => $urls,
        ]);
    }

    /**
     * @Route("/admin/view_orders", name="admin_view_orders")
     */
    public function viewOrders(): Response
    {
        $orders = $this->getDoctrine()->getManager()->getRepository(Orders::class)->findAll();

        return $this->render('admin/viewOrders.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/admin/view_users", name="admin_view_users")
     */
    public function viewUsers(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('admin/viewUsers.html.twig', [
            'users' => $users,
        ]);
    }
}
