<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function index(Request $request): Response
    {
        $searchQuery = $request->get('query');
        $searchCategory = (int) ($request->get('category'));
        $results = [];
        if (!$searchQuery) {
            return $this->render('search/index.html.twig', [
                'results' => $results,
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        if ($searchCategory) {
            $items = $em->getRepository(Item::class)->createQueryBuilder('i')
                ->leftJoin('i.Item_Category_ID', 'item_category_id')
                ->where('i.name LIKE :query')
                ->andWhere('item_category_id = :category')
                ->setParameter('query', '%' . $searchQuery . '%')
                ->setParameter('category', $searchCategory)
                ->getQuery()
                ->getResult();
        } else {
            $items = $em->getRepository(Item::class)->createQueryBuilder('i')
                ->where('i.name LIKE :query')
                ->setParameter('query', '%' . $searchQuery . '%')
                ->getQuery()
                ->getResult();
        }

        if ($items) {
            foreach ($items as $item) {
                $results[] = $item;
            }
        }

        return $this->render('search/index.html.twig', [
            'results' => $results,
        ]);
    }
}
