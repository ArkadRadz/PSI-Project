<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\Item;
use App\Entity\Orders;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasketController extends AbstractController
{
    /**
     * @Route("/basket", name="basket")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if (!$user) {
            return $this->render('error.html.twig', [
                'exceptionTitle' => 'not logged in',
                'exception' => 'this site is for users only',
            ]);
        }

        /** @var Orders $order */
        $order = $em->getRepository(Orders::class)->createQueryBuilder('o')
            ->where('o.isFinished = :order_state')
            ->andWhere('o.User_ID = :userID')
            ->setParameter('order_state', false)
            ->setParameter('userID', $user->getId())
            ->orderBy('o.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        $orderRows = [];
        $items = [];

        if (!$order) {
            return $this->render('error.html.twig', [
                'exceptionTitle' => 'no order',
                'exception' => 'basket is empty',
            ]);
        }
        $orderItems = $order->getOrderRowID()->current();

        foreach ($order->getOrderRowID() as $orderRow) {
            $orderRows[] = $orderRow;
        }

        if (empty($orderItems)) {
            return $this->render('error.html.twig', [
                'exceptionTitle' => 'cart empty',
                'exception' => 'cart is empty',
            ]);
        }

        /** @var Item $orderItem */
        foreach ($orderItems->getItemID() as $orderItem) {
            $items[] = $orderItem;
        }

        return $this->render('basket/index.html.twig', [
            'orderRow' => $orderRows,
        ]);
    }
}
