<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\ItemCategory;
use App\Form\ItemCategoryFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemCategoryController extends AbstractController
{
    /**
     * @Route("/category/new", name="item_category_create")
     */
    public function addNewCategory(Request $request): Response
    {
        $form = $this->createForm(ItemCategoryFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $itemCategory = new ItemCategory();
            /** @var ItemCategory $data */
            $data = $form->getData();

            $itemCategory->setCategory($data->getCategory());

            foreach ($data->getItemID() as $item) {
                $itemCategory->addItemID($item);
            }

            $em->persist($itemCategory);
            $em->flush();
            //add flash https://symfony.com/doc/current/controller.html#flash-messages

            return $this->redirectToRoute('item_categories');
        }

        return $this->render('item_category/createItemCategory.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/categories", name="item_categories")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $itemCategories = $em->getRepository(ItemCategory::class)->findAll();

        return $this->render('item_category/index.html.twig', [
            'item_categories' => $itemCategories,
        ]);
    }

    /**
     * @Route("/itemcategory/{itemCategoryID}", name="itemcategory_read", requirements={"itemCategoryID"="\d+"})
     */
    public function viewSingleCategory(int $itemCategoryID): Response
    {
        $em = $this->getDoctrine()->getManager();

        $itemCategory = $em->getRepository(ItemCategory::class)->find($itemCategoryID);

        if (!$itemCategory) {
            return $this->render('error.html.twig', [
                'exception' => 'not found',
                'exceptionTitle' => 'error',
            ]);
        }

        return $this->render('item_category/viewItemCategory.html.twig', [
            'item_category' => $itemCategory,
        ]);
    }
}
