<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\Orders;
use App\Form\OrderType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CheckoutController extends AbstractController
{
    /**
     * @Route("/checkout", name="checkout")
     */
    public function index(Request $request): Response
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->render('error.html.twig', [
                'exception' => 'please login first',
                'exceptionTitle' => 'not authorised',
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $orderForm = $this->createForm(OrderType::class);

        /** @var Orders $order */
        $order = $em->getRepository(Orders::class)->createQueryBuilder('o')
            ->where('o.isFinished = :order_state')
            ->andWhere('o.User_ID = :userID')
            ->setParameter('order_state', false)
            ->setParameter('userID', $user->getId())
            ->orderBy('o.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        $orderForm->handleRequest($request);

        if ($orderForm->isSubmitted() && $orderForm->isValid()) {
            $data = $orderForm->getData();
            $order->setIsFinished(true);
            $order->setOrderDate(date('d/m/y H:i:s'));
            $order->setDeliveryCity($data['delivery_city']);
            $order->setDeliveryCountry($data['delivery_country']);
            $order->setDeliveryPostcode($data['delivery_postcode']);
            $order->setDeliveryStreet($data['delivery_street']);
            $em->persist($order);
            $em->flush();

            return $this->render('checkout/thankyou.html.twig', [
                'orderid' => $order->getId(),
            ]);
        }

        return $this->render('checkout/index.html.twig', [
            'form' => $orderForm->createView(),
        ]);
    }
}
