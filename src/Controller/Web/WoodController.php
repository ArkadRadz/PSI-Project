<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\Wood;
use App\Form\WoodFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WoodController extends AbstractController
{
    /**
     * @Route("/woods", name="woods_read")
     */
    public function getWoods(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $woods = $em->getRepository(Wood::class)->findAll();

        if (0 === count($woods)) {
            return $this->render('error.html.twig', [
                'exception' => 'no woods',
                'exceptionTitle' => 'error',
            ]);
        }

        return $this->render('wood/woods.html.twig', [
            'woods' => $woods,
        ]);
    }

    /**
     * @Route("/admin/wood/new", name="wood_create")
     */
    public function createWood(Request $request): Response
    {
        $form = $this->createForm(WoodFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $data = $form->getData();
            $wood = new Wood();
            $wood->setWood($data->getWood());

            $em->persist($wood);
            $em->flush();
            //add flash https://symfony.com/doc/current/controller.html#flash-messages

            return $this->redirectToRoute('woods_read');
        }

        return $this->render('wood/createWood.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/wood/{id}", name="wood_edit", requirements={"id"="\d+"})
     */
    public function editWood(Request $request, int $id): Response
    {
        $em = $this->getDoctrine();
        $wood = $em->getRepository(Wood::class)->find($id);

        $form = $this->createForm(WoodFormType::class, $wood);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $wood = $data;
            $em->getManager()->persist($wood);
            $em->getManager()->flush();
        }

        return $this->render('wood/createWood.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
