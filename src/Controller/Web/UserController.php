<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\Orders;
use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user/{uuid}", name="user", requirements={"uuid"="\d+"})
     */
    public function index(int $uuid): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($uuid);

        if (!$user) {
            return $this->render('error.html.twig', [
                'exception' => 'not found',
                'exceptionTitle' => 'error',
            ]);
        }

        return $this->render('user/index.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/my_orders", name="user_orders")
     */
    public function myOrders(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            return $this->render('error.html.twig', [
                'exceptionTitle' => 'not logged in',
                'exception' => 'this page is for registered users only',
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $orders = $em->getRepository(Orders::class)->createQueryBuilder('o')
            ->where('o.User_ID = :userid')
            ->setParameter('userid', $user->getId())
            ->getQuery()
            ->execute()
        ;

        return $this->render('user/userOrders.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/user/edit", name="user_edit")
     */
    public function editUser(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $userData = $em->getRepository(User::class)->find($user->getId());

        if (!$userData) {
            return $this->render('error.html.twig', [
                'exception' => 'Not correct user',
                'exceptionTitle' => 'Invalid user',
            ]);
        }

        $editForm = $this->createForm(RegistrationFormType::class, $userData);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $data = $editForm->getData();

            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('user/userEdit.html.twig', [
            'editForm' => $editForm->createView(),
        ]);
    }
}
