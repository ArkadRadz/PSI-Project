<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\Wishlist;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WishlistController extends AbstractController
{
    /**
     * @Route("/wishlist", name="wishlist")
     */
    public function index(): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->render('error.html.twig', [
                'exceptionTitle' => 'not logged in',
                'exception' => 'you need to be logged in to access this website',
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $userWishlist = $em->getRepository(Wishlist::class)->findBy(['user' => $user]);

        return $this->render('wishlist/index.html.twig', [
            'wishlistItems' => $userWishlist,
        ]);
    }
}
