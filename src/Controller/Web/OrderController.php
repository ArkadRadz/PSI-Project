<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\Orders;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route("/order/{orderID}", name="order_read", requirements={"orderID"="\d+"})
     */
    public function viewSingleOrder(int $orderID): Response
    {
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository(Orders::class)->find($orderID);

        if (!$order) {
            return $this->render('error.html.twig', [
                'exception' => 'not found',
                'exceptionTitle' => 'error',
            ]);
        }

        return $this->render('order/index.html.twig', [
            'order' => $order,
        ]);
    }
}
