<?php

declare(strict_types=1);

namespace App\Controller\Web;

use App\Entity\Image;
use App\Entity\Item;
use App\Entity\Orders;
use App\Entity\Wood;
use App\Form\ItemEditFormType;
use App\Form\ItemFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemController extends AbstractController
{
    /**
     * @Route("/admin/item/new", name="item_create")
     */
    public function createItem(Request $request): Response
    {
        $woods = $this->getDoctrine()->getManager()->getRepository(Wood::class)->findAll();

        if (0 === count($woods)) {
            return $this->render('error.html.twig', [
                'exception' => 'no woods <a href="' . $this->generateUrl('wood_create') . '">click here to create new',
                'exceptionTitle' => 'no woods',
            ]);
        }

        $form = $this->createForm(ItemFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $allowedImageFormats = $this->getParameter('allowed_image_formats');

            /** @var Item $item */
            $item = new Item();
            $data = $form->getData();

            $item->setName($data['name']);
            $item->setDescription($data['description']);
            $item->setHeight($data['height']);
            $item->setWidth($data['width']);
            $item->setItemAmount($data['item_amount']);
            $item->setItemPrice($data['item_price']);

            /** @var UploadedFile $file */
            foreach ($data['images'] as $file) {
                if (in_array($file->guessExtension(), $allowedImageFormats)) {
                    $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();

                    try {
                        $file->move(
                            $this->getParameter('image_location'),
                            $fileName
                        );
                    } catch (FileException $e) {
                        // ... handle exception if something happens during file upload
                    }

                    $image = new Image();
                    $image->setSource($fileName);
                    $em->persist($image);
                    $item->addImage($image);
                }
            }

            foreach ($data['wood'] as $wood) {
                $item->addWoodID($wood);
            }

            $em->persist($item);
            $em->flush();

            return $this->redirectToRoute('items_read');
        }

        return $this->render('item/createItem.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/item/{itemID}", name="item_edit", requirements={"itemID"="\d+"})
     */
    public function editItem(int $itemID, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository(Item::class)->find($itemID);

        if (!$item) {
            return $this->render('error.html.twig', [
                'exception' => 'not found',
                'exceptionTitle' => 'error',
            ]);
        }
        $form = $this->createForm(ItemEditFormType::class);
        $form->get('name')->setData($item->getName());
        $files = [];
        foreach ($item->getImages() as $image) {
            $files[] = new File($this->getParameter('image_location') . '/' . $image->getSource());
        }
        $form->get('description')->setData($item->getDescription());
        $form->get('width')->setData($item->getWidth());
        $form->get('height')->setData($item->getHeight());
        $form->get('length')->setData($item->getLength());
        $form->get('item_amount')->setData($item->getItemAmount());
        $form->get('item_price')->setData($item->getItemPrice());
        $form->get('wood')->setData($item->getWoodID());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $allowedImageFormats = $this->getParameter('allowed_image_formats');
            $data = $form->getData();
//            foreach ($item->getWoodID() as $wood) {
//                $item->removeWoodID($wood);
//            }
//            foreach ($item->getImages() as $image) {
//                $item->removeImage($image);
//            }
            $item->setName($data['name']);
            $item->setDescription($data['description']);
            $item->setHeight($data['height']);
            $item->setWidth($data['width']);
            $item->setLength($data['length']);
            $item->setItemAmount($data['item_amount']);
            $item->setItemPrice($data['item_price']);
            foreach ($data['images'] as $file) {
                if ($file instanceof UploadedFile) {
                    /** @var UploadedFile $file */
                    if (in_array($file->guessExtension(), $allowedImageFormats)) {
                        $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();

                        try {
                            $file->move(
                                $this->getParameter('image_location'),
                                $fileName
                            );
                        } catch (FileException $e) {
                            // ... handle exception if something happens during file upload
                        }

                        $image = new Image();
                        $image->setSource($fileName);
                        $em->persist($image);
                        $item->addImage($image);
                    }
                }
            }

            foreach ($data['wood'] as $wood) {
                $item->addWoodID($wood);
            }

            $em->persist($item);
            $em->flush();

            return $this->redirectToRoute('items_read');
        }

        return $this->render('item/editItem.html.twig', [
            'form' => $form->createView(),
            'images' => $item->getImages(),
            'item_id' => $item->getId(),
        ]);
    }

    /**
     * @Route("/item/{itemID}", name="item_read", requirements={"itemID"="\d+"})
     */
    public function getItem(int $itemID): Response
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository(Item::class)->find($itemID);

        if (!$item) {
            return $this->render('error.html.twig', [
                'exception' => 'not found',
                'exceptionTitle' => 'error',
            ]);
        }

        return $this->render('item/singleItem.html.twig', [
            'item' => $item,
        ]);
    }

    /**
     * @Route("/items", name="items_read")
     */
    public function getItems(): Response
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $items = $em->getRepository(Item::class)->findAll();

        $order = '';

        if ($user) {
            $order = $em->getRepository(Orders::class)->createQueryBuilder('userOrder')
                ->where('userOrder.isFinished = false')
                ->andWhere('userOrder.User_ID = :user')
                ->setParameter('user', $user)
                ->getQuery()
                ->getOneOrNullResult()
            ;
        }

        if (0 === count($items)) {
            return $this->render('error.html.twig', [
                'exception' => 'no items <a href="' . $this->generateUrl('item_create') . '">click here to create new',
                'exceptionTitle' => 'error',
            ]);
        }

        return $this->render('item/items.html.twig', [
            'items' => $items,
            'image_path' => $this->getParameter('image_location'),
            'order' => $order,
        ]);
    }

    private function generateUniqueFileName(): string
    {
        return md5(uniqid());
    }
}
