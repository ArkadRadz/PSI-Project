<?php

declare(strict_types=1);

namespace App\Twig;

use App\Entity\OrderRow;
use App\Entity\Orders;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class BasketItemAmountExtension extends AbstractExtension
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_basket_amount', [$this, 'returnItemAmount']),
        ];
    }

    public function returnItemAmount(User $user)
    {
        $query = $this->entityManager->getRepository(Orders::class)->createQueryBuilder('o')
            ->where('o.isFinished = :order_state')
            ->andWhere('o.User_ID = :userID')
            ->setParameter('order_state', false)
            ->setParameter('userID', $user->getId())
            ->orderBy('o.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
        ;

        $order = '';
        $orderRows = [];

        try {
            $order = $query->getOneOrNullResult();
        } catch (\Exception $exception) {
            return 0;
        }

        $count = 0;

        if ($order) {
            $orderRows = $order->getOrderRowID();
        }

        /** @var OrderRow $orderRow */
        foreach ($orderRows as $orderRow) {
            $count += $orderRow->getQuantity();
        }

        return $count;
    }
}
