<?php

namespace App\Twig;

use App\Entity\Orders;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class GetOrderAmountTwigExtension extends AbstractExtension
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_order_amount', [$this, 'getOrderAmount']),
        ];
    }

    public function getOrderAmount(User $user)
    {
        $query = $this->entityManager->getRepository(Orders::class)->createQueryBuilder('o')
            ->where('o.isFinished = :order_state')
            ->andWhere('o.User_ID = :userID')
            ->setParameter('order_state', false)
            ->setParameter('userID', $user->getId())
            ->orderBy('o.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
        ;

        try {
            /** @var Orders $order */
            $order = $query->getOneOrNullResult();
            if ($order) {
                return $order->getSumPrice();
            } else {
                return 0;
            }

        } catch (\Exception $exception) {
            return 0;
        }
    }
}
