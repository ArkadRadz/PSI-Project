<?php

declare(strict_types=1);

namespace App\Twig;

use App\Entity\User;
use App\Entity\Wishlist;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class WishlistAmountTwigExtension extends AbstractExtension
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getWishlists', [$this, 'getWishlists']),
        ];
    }

    public function getWishlists(User $user)
    {
        $wishlists = $this->entityManager->getRepository(Wishlist::class);

        $userWishlists = $wishlists->findBy(['user' => $user]);

        $count = 0;

        foreach ($userWishlists as $userWishlist) {
            ++$count;
        }

        return $count;
    }
}
