<?php

declare(strict_types=1);

namespace App\Twig;

use App\Entity\ItemCategory;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ItemCategoryTwigExtension extends AbstractExtension
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_categories', [$this, 'getCategories']),
            new TwigFunction('get_categories_with_count', [$this, 'getCategoriesWithCount']),
        ];
    }

    public function getCategories()
    {
        return $this->entityManager->getRepository(ItemCategory::class)->findAll();
    }

    public function getCategoriesWithCount()
    {
        $result = [];
        $itemCategories = $this->entityManager->getRepository(ItemCategory::class)->findAll();

        foreach ($itemCategories as $itemCategory) {
            $itemsCount =  $itemCategory->getItemID()->count();
            $result[] = [
                'category' => $itemCategory,
                'count' => $itemsCount,
            ];
        }

        return $result;
    }
}
