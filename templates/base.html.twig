<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{% block title %}{% endblock %}</title>
        <meta name="description" content="">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="{{ asset('apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}">

		{% block styles %}
		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
		<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>

        <!-- Google font -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">

        <link rel="stylesheet" href="{{ asset('css/main.css') }}">

        <script src="{{ asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>

        <!-- Slick -->
		<link type="text/css" rel="stylesheet" href="{{ asset('css/slick.css') }}"/>
		<link type="text/css" rel="stylesheet" href="{{ asset('css/slick-theme.css') }}"/>

		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="{{ asset('css/nouislider.min.css') }}"/>

        <!-- Font Awesome Icon -->
		<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
		{% endblock %}

    </head>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><i class="fa fa-phone"></i> +123-456-789</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> sklep@carpentrex.com</a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> ul. Prószkowska 1 Opole</a></li>
					</ul>
					<ul class="header-links pull-right">
						{% if is_granted('ROLE_ADMIN') %}
						<li>
							<i class="fa fa-user"></i>
							<a href="{{ path('admin') }}">Admin panel</a>
						</li>
						{% endif %}
						<li>
							{% if app.user %}
							<i class="fa fa-user"></i>
							<a href="{{ path('user_edit') }}">Edit account</a>
							{% else %}
							<i class="fa fa-dollar"></i>
							<a href="{{ path('register') }}">Register</a>
							{% endif %}
						</li>
						{% if app.user %}
						<li>
							<i class="fa fa-shopping-basket"></i>
							<a href="{{ path('user_orders') }}">My orders</a>
						</li>
						{% endif %}
						<li>
							{% if app.user %}
							<i class="fa fa-sign-out"></i>
							<a href="{{ path('logout') }}">Logout</a>
							{% else %}
							<i class="fa fa-user"></i>
							<a href="{{ path('login') }}">Login</a>
							{% endif %}
            			</li>
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="/" class="logo">
									{# <img src="./img/logo.png" alt=""> #}
									<h2 style="margin-top: 15px; color:gainsboro">Carpentrex</h2>
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form action="{{ path('search') }}" id="searchform" method="GET">
									<select class="input-select" name="category" form="searchform" style="max-width:40%">
										<option value="0">All Categories</option>
										{% set item_categories = get_categories() %}
										{% for item_category in item_categories %}
											<option value="{{ item_category.id }}">{{ item_category.category }}</option>
										{% endfor %}
									</select>
									<input class="input" name="query" placeholder="Search here" style="max-width:40%">
									<button class="search-btn" style="max-width:20%">Search</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
						<div class="col-md-3 clearfix">
							<div class="header-ctn">
								<!-- Wishlist -->
								<div>
									<a id="wishlist_amount_link" href="{{ path('wishlist') }}">
										<i class="fa fa-heart-o"></i>
										<span>Your Wishlist</span>
										{% if app.user %}
											{% set wishlistAmount = getWishlists(app.user) %}
											{% if wishlistAmount > 0 %}
										<div id="wishlist_amount" class="qty">{{ wishlistAmount }}</div>
											{% endif %}
										{% endif %}
									</a>
								</div>
								<!-- /Wishlist -->

								<!-- Cart -->
								<div class="dropdown">
									<a id="cart_amount_link" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
										<i class="fa fa-shopping-cart"></i>
										<span>Your Cart</span>
										{% if app.user %}
											{% set amount = get_basket_amount(app.user) %}
											{% if amount > 0 %}
										<div id="cart_amount" class="qty">{{ amount }}</div>
											{% endif %}
										{% endif %}
									</a>
									<div class="cart-dropdown">
										<div class="cart-summary-no-top-border">
										{% if app.user %}
											{% set amount = get_basket_amount(app.user) %}
											{% if amount > 0 %}
												<small><span class="cart-summary-amount">{{ amount }}</span> Item(s) selected</small>
											{% endif %}
											<h5>SUBTOTAL: <span class="cart-summary-subtotal">{{ get_order_amount(app.user) }} ZŁ</span></h5>
										{% endif %}
											
											
										</div>
										<div class="cart-btns">
											<a href="{{ path('basket') }}">View Cart</a>
											<a href="{{ path('checkout') }}">Checkout  <i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
								</div>
								<!-- /Cart -->

								<!-- Menu Toogle -->
								<div class="menu-toggle">
									<a href="#">
										<i class="fa fa-bars"></i>
										<span>Menu</span>
									</a>
								</div>
								<!-- /Menu Toogle -->
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->

		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
						<li class="active"><a href="/">Home</a></li>
						<li><a href="/woods">Woods</a></li>
						<li><a href="{{ path('items_read') }}">Items</a></li>
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
          {% block body %}{% endblock %}
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->


		<!-- FOOTER -->
		<footer id="footer" style="text-align:center;">
			<!-- top footer -->
			<div class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
					{# <div class="col-md-3 col-xs-6"></div> #}
						<div class="col-md-6 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">About Us</h3>
								<p>Carpentrex - wszystko czego potrzebujesz z drewna</p>
								<ul class="footer-links">
									<li><a href="#"><i class="fa fa-phone"></i> +123-456-789</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> sklep@carpentrex.com</a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> ul. Prószkowska 1 Opole</a></li>
								</ul>
							</div>
						</div>
						{% if item_categories is not empty %}
						<div class="col-md-6 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Categories</h3>
								<ul class="footer-links">
									{% for item_category in item_categories %}
									<li><a href="{{ path('itemcategory_read', {'itemCategoryID': item_category.id}) }}">{{ item_category }}</a></li>
									{% endfor %}
								</ul>
							</div>
						</div>
						{% endif %}
						<div class="clearfix visible-xs"></div>
{# <div class="col-md-3 col-xs-6"></div> #}
						{# <div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Information</h3>
								<ul class="footer-links">
									<li><a href="#">About Us</a></li>
									<li><a href="#">Contact Us</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Orders and Returns</a></li>
									<li><a href="#">Terms & Conditions</a></li>
								</ul>
							</div>
						</div> #}

						{# <div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Service</h3>
								<ul class="footer-links">
									{% if app.user %}
									<li><a href="{{ path('user_orders') }}">My Account</a></li>
									{% endif %}
									<li><a href="{{ path('basket') }}">View Cart</a></li>
									<li><a href="{{ path('wishlist') }}">Wishlist</a></li>
									<li><a href="{{ path('user_orders') }}">View My Orders</a></li>
									<li><a href="#">Help</a></li>
								</ul>
							</div>
						</div> #}
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /top footer -->

			<!-- bottom footer -->
			<div id="bottom-footer" class="section">
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-12 text-center">
							<ul class="footer-payments">
								<li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
								<li><a href="#"><i class="fa fa-credit-card"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
							</ul>
							<span class="copyright">
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
								Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							</span>
						</div>
					</div>
						<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /bottom footer -->
		</footer>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/slick.min.js') }}"></script>
		<script src="{{ asset('js/nouislider.min.js') }}"></script>
		<script src="{{ asset('js/jquery.zoom.min.js') }}"></script>
		<script src="{{ asset('js/main.js') }}"></script>

	</body>
</html>
