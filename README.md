# PSI-Project
PSI Project.

# Setup
1. After cloning, run `composer install`
2. To start the server run `bin/console server:start` or `bin/console server:run`
3. Verify and install the requirements shown by navigating to `http://127.0.0.1:8000/check.php` (or other address shown after running the command above) 
4. Once finished run following commands

>  `bin/console doctrine:database:create`

>  `bin/console doctrine:schema:create`

>  `bin/console doctrine:fixtures:load --append`

5. 
6. To access Admin account, use `admin\admin`

# Code style

1. Use Yoda style comparisons
```php
if ($variable == 42)
```
is incorrect.
```php
if (42 == $variable)
```
is correct.

2. 1 issue == 1 branch!
3. Every new feature should be tested using `PHPUnit`.
4. Every site functionality should be tested using `Behat`.
5. Branch `master` is only for production. Push all fixes/features to branch `develop` via `Merge Request`!
6. Before creating a `Merge Request`, run EasyCodingStandard to ensure quality of the code.
